﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using System.IO;
using System.Text;
using Tomlyn;
using Tomlyn.Model;
using Bridge.Irc;
using Bridge.Log;

namespace Bridge
{
    class Program
    {
        // Discord
        private DiscordSocketClient _client;
        private string _discordPrefix; // Prefix that the discord bot will listen for
        public static bool dConnected = false; // whether or not the discord client is ready

        // IRC
        public static Client ircClient;
        private string _username;

        // Logging
        public static Logger logger;

        // bridge
        public static Bridge[] bridges; // Array of the potential multiple bridges, each bridge containing the discord channels and irc channels they bridge

        // Statistics
        public static Statistics stats; // Statistics for both the discord and irc side of the bridge

        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private async Task MainAsync()
        {
            // checks if config file exists
            if (!File.Exists("config.toml"))
            {
                Console.WriteLine("Could not find config file, exiting...");
                return;
            }

            // Irc variables which are passed into the initialisation of the irc client
            string token, hostname, ircPrefix;
            int port, timeout;
            bool tls, ignoreInvalidCert;

            // logging variables
            LoggerLevel level;
            LoggerType type;
            string path;
            try
            {
                var config = Toml.Parse(await File.ReadAllTextAsync("config.toml")).ToModel();
                TomlTable dt = (TomlTable)config["Discord"];
                TomlTable irct = (TomlTable)config["IRC"];
                TomlTable logt = (TomlTable)config["Logging"];
                TomlTableArray bridge = (TomlTableArray)config["Bridge"];

                // Discord table
                token = (string)dt["token"];
                _discordPrefix = (string)dt["commandPrefix"];

                // IRC table
                _username = (string)irct["username"];
                hostname = (string)irct["hostname"];
                port = (int)(long)irct["port"];
                tls = (bool)irct["tls"];
                timeout = (int)(long)irct["timeout"];
                ignoreInvalidCert = (bool)irct["ignoreInvalidCert"];
                ircPrefix = (string)irct["commandPrefix"];

                // Logging table
                path = (string)logt["path"];
                level = ParseEnum<LoggerLevel>((String)logt["level"]);
                type = ParseEnum<LoggerType>((String)logt["type"]);

                // Initialisation of the bridge array, adding the Bridge objects to it
                bridges = new Bridge[bridge.Count];
                for (int i = 0; i < bridge.Count; i++)
                {
                    Bridge b = new Bridge((string)bridge[i]["irc"], (long)bridge[i]["discord"]);
                    bridges[i] = b;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }

            logger = new Logger(path, type, level);
            stats = new Statistics();

            // Initialisation of the irc client, and adding irc event handlers
            ircClient = new Client(_username, hostname, port, timeout, tls, ignoreInvalidCert, ircPrefix);
            ircClient.OnMessage += OnIrcMessageReceive;
            ircClient.OnReady += OnIrcReady;
            ircClient.OnUserJoin += OnIrcUserJoin;
            ircClient.OnUserPart += OnIrcUserPart;
            ircClient.OnCommand += OnIrcCommand;
            ircClient.OnUserPart += OnIrcUserPart;
            ircClient.OnUserQuit += OnIrcUserQuit;
            ircClient.OnNamesEvent += OnIrcNamesEvent;

            // Discord client initalisation and adding of discord event handlers
            _client = new DiscordSocketClient();
            _client.Log += message =>
            {
                switch (message.Severity)
                {
                    case LogSeverity.Info:
                        logger.Info(message.Message);
                        break;

                    case LogSeverity.Critical:
                    case LogSeverity.Error:
                        logger.Error(message.Message);
                        break;

                    case LogSeverity.Debug:
                    case LogSeverity.Verbose:
                        logger.Debug(message.Message);
                        break;

                    case LogSeverity.Warning:
                        logger.Warn(message.Message);
                        break;
                }
                return Task.CompletedTask;
            };
            _client.Ready += OnReady;
            _client.MessageReceived += OnDiscordMessageReceive;

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            try
            {
                ircClient.Start();  // blocks main thread to stop program exiting
            } catch (Exception e)
            {
                logger.Error("An unexpected error has occurred:\n" + e.ToString());
                Environment.Exit(1);
            }
        }

        private Task OnReady() // invoked when the Discord.Net successfully connects to the discord API
        {
            logger.Info("Successfully logged in as " + _client.CurrentUser);
            dConnected = true;
            return Task.CompletedTask;
        }

        private Task OnDiscordMessageReceive(SocketMessage message) // invoked when a discord message gas been received
        {
            if (message.Author.IsWebhook || (message.Author.Id == _client.CurrentUser.Id) || (message.Content.Length == 0 && !(message.Attachments.Count > 0))) return Task.CompletedTask; // prevents webhooks and the bot itself from invoking commands or being relayed to IRC
            // checks if the message is a command, and invokes the command handler
            if (message.Attachments.Count > 0)
            {
                foreach (Attachment attachment in message.Attachments)
                {
                    foreach (Bridge bridge in bridges)
                    {
                        if (bridge.discordChannel == (long)message.Channel.Id)
                        {
                            string channel = bridge.ircChannel;
                            float size = (float)Math.Round((double)attachment.Size / 1024 / 1024, 2);
                            string nick = ((SocketGuildUser)message.Author).Nickname;
                            if (nick == null)
                            {
                                nick = message.Author.Username;
                            }
                            string content = $"[#{message.Channel.Name}][{attachment.Filename}: {size}MB]: <{nick}> {attachment.Url}";
                            ircClient.SendCommand($"PRIVMSG {channel} :{content}");
                        }
                    }
                }
                return Task.CompletedTask;
            }
            if (message.Content.StartsWith(_discordPrefix))
            {
                DiscordCommandHandler.Handle(_discordPrefix, message);
            }
            foreach (Bridge bridge in bridges)
            {
                if (bridge.discordChannel == (long)message.Channel.Id)
                {
                    string channel = bridge.ircChannel;
                    string nick = ((SocketGuildUser)message.Author).Nickname;
                    if (nick == null)
                    {
                        nick = message.Author.Username;
                    }
                    string content = $"[#{message.Channel.Name}]: <{nick}> {message.Content}";
                    ircClient.SendCommand($"PRIVMSG {channel} :{content}");
                    logger.Debug("[Discord --> IRC] " + content);
                }
                stats.discordMessages += 1;
                stats.discordMessageChars += message.Content.Length;
                stats.discordMessageWords += Statistics.CalculateWords(message.Content);
                return Task.CompletedTask;
            }
            return Task.CompletedTask;
        }

        private void OnIrcMessageReceive(string author, string channel, string msg) // invoked when an irc message has been received
        {
            foreach (Bridge bridge in bridges)
            {
                ulong id = (ulong)bridge.discordChannel;
                if (bridge.ircChannel == channel)
                {
                    string parsedMsg = ParseDiscordMessage(id, msg);
                    DateTime time = DateTime.Now;
                    string content = $"[{time.ToShortTimeString()}][{channel}]: <{author}> {parsedMsg}";
                    ITextChannel dchannel = (ITextChannel)_client.GetChannel(id);
                    if (dchannel == null)
                    {
                        logger.Error($"Failed to fetch discord channel with id {id} in OnIrcMessageReceived");
                        return;
                    }
                    dchannel.SendMessageAsync(content);
                    logger.Debug("[IRC --> Discord]: " + content);
                }
            }
            stats.ircMessages += 1;
            stats.ircMessageChars += msg.Length;
            stats.ircMessageWords += Statistics.CalculateWords(msg);
        }

        private void OnIrcCommand(string author, string channel, char prefix, string command, string sargs) // invoked when the irc message begins with the command prefix
        {
            switch (command.ToLower())
            {
                case "stats":
                    ircClient.SendCommand($"PRIVMSG {channel} :---- Bot Statistics ----");
                    ircClient.SendCommand($"PRIVMSG {channel} :[IRC] messages: {stats.ircMessages} words: {stats.ircMessageWords} average w/m: {stats.AverageIrcWords()} characters: {stats.ircMessageChars} average c/m: {stats.AverageIrcChars()}");
                    ircClient.SendCommand($"PRIVMSG {channel} :[Discord] messages: {stats.discordMessages} words: {stats.discordMessageWords} average w/m: {stats.AverageDiscordWords()} characters: {stats.discordMessageChars} average c/m: {stats.AverageDiscordChars()}");
                    ircClient.SendCommand($"PRIVMSG {channel} :");
                    break;
            }
        }

        private void OnIrcReady() // Invoked when code 001 is received, indicating a successful connection
        {
            foreach (Bridge bridge in bridges)
            {
                string channel = bridge.ircChannel;
                // Join the channels defined in the config file
                ircClient.SendCommand("JOIN " + channel);
                logger.Info("Joined IRC channel: " + channel);
            }
        }

        private void OnIrcUserJoin(string user, string channel) // Invoked when a user joins an IRC channel in which the bot is in
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithTitle($"{user} has joined {channel}");
            builder.WithColor(Color.Green);
            ulong id = 0;
            foreach (Bridge bridge in bridges)
            {
                if (bridge.ircChannel == channel)
                {
                    id = (ulong)bridge.discordChannel;
                }
            }
            SendDiscordEmbed(builder.Build(), id);
            logger.Debug($"{user} has joined {channel}");
        }

        private void OnIrcUserPart(string user, string channel, string message) // Invoked when a user leaves an IRC channel in which the bot is in
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithTitle($"{user} has left {channel} ({message})");
            builder.WithColor(Color.Red);
            ulong id = 0;
            foreach (Bridge bridge in bridges)
            {
                if (bridge.ircChannel == channel)
                {
                    id = (ulong)bridge.discordChannel;
                }
            }
            SendDiscordEmbed(builder.Build(), id);
            logger.Debug($"{user} has left {channel}");
        }

        private void OnIrcUserQuit(string user, string message) // Invoked when a user within an IRC channel which the bot is in terminates their connection
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithTitle($"{user} has quit ({message})");
            builder.WithColor(Color.Red);
            logger.Debug($"{user} has quit");
        }

        private void OnIrcNamesEvent(string channel, string[] users) // Invoked when the status code 353 is received
        {
            StringBuilder builder = new StringBuilder();
            foreach (string user in users)
            {
                builder.Append($"{user}\n");
            }
            builder.Append($"{channel} contains {users.Length} users");
            EmbedBuilder ebuilder = new EmbedBuilder();
            ebuilder.WithTitle($"Users in {channel}:");
            ebuilder.WithDescription(builder.ToString());
            ebuilder.WithColor(Color.Green);
            ulong id = 0;
            foreach (Bridge bridge in bridges)
            {
                if (bridge.ircChannel == channel)
                {
                    id = (ulong)bridge.discordChannel;
                }
            }
            SendDiscordEmbed(embed: ebuilder.Build(), id);
        }

        private string ParseDiscordMessage(ulong channelId, string message) // Parses the message received from IRC before sending it is sent to discord
        {
            SocketTextChannel channel = (SocketTextChannel)_client.GetChannel(channelId);
            if (channel == null)
            {
                logger.Error($"Could not get discord channel with the id {channelId} in ParseDiscordMessage");
                return message;
            }
            string[] args = message.Split(" ");
            int i = 0;
            foreach (string arg in args)
            {
                if (arg.StartsWith("@"))
                {
                    if (arg.ToLower() == "@everyone" || arg.ToLower() == "@here")
                    {
                        args[i] = "";
                        continue;
                    }
                    foreach (IGuildUser user in channel.Users)
                    {
                        String name = user.Nickname;
                        if (name == null)
                        {
                            name = user.Username;
                        }
                        Console.WriteLine(user.Nickname);
                        Console.WriteLine(user.Username);
                        if (arg == "@" + name)
                        {
                            args[i] = user.Mention;
                        }
                    }
                }
                i++;
            }
            return string.Join(" ", args);
        }

        private void SendDiscord(string message)
        {
            foreach (Bridge bridge in bridges)
            {
                long id = bridge.discordChannel;
                IMessageChannel dchannel = (IMessageChannel)_client.GetChannel((ulong)id);
                if (dchannel == null)
                {
                    logger.Error($"Failed to fetch discord channel, id: {id}");
                    return;
                }
                dchannel.SendMessageAsync(message);
            }
        }

        private void SendDiscordEmbed(Embed embed, ulong channel)
        {
            IMessageChannel dchannel = (IMessageChannel)_client.GetChannel(channel);
            if (dchannel == null)
            {
                logger.Error($"Failed to fetch discord channel, id: {channel}");
                return;
            }
            dchannel.SendMessageAsync(embed: embed);
        }

        private T ParseEnum<T>(string value) // Parses a value to the enum T
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
