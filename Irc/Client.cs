using System;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using static Bridge.Program;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Bridge.Irc
{
    class ParsedMessage
    {
        public string command { get; set; }
        public string author { get; set; }
        public string content { get; set; }
        public string channel { get; set; }
        public string[] users { get; set; }

        public ParsedMessage(string command = null, string author = null, string content = null)
        {
            this.command = command;
            this.author = author;
            this.content = content;
        }
    }

    public class Client
    {
        public string host { get; }
        public string username { get; }
        public string prefix { get; set; }
        public int port { get; }
        public int timeout { get; set; }

        private TcpClient _client;
        private TextWriter _tx;
        private StreamReader _rx;
        private bool _tls, _ignoreInvalidCert, _connected;

        public delegate void MessageEvent(string author, string channel, string msg);
        public event MessageEvent OnMessage;

        public delegate void ReadyEvent();
        public event ReadyEvent OnReady;

        public delegate void JoinEvent(string user, string channel);
        public event JoinEvent OnUserJoin;

        public delegate void PartEvent(string user, string channel, string message);
        public event PartEvent OnUserPart;

        public delegate void CommandEvent(string author, string channel, char prefix, string command, string args);
        public event CommandEvent OnCommand;

        public delegate void QuitEvent(string user, string message);
        public event QuitEvent OnUserQuit;

        public delegate void NamesEvent(string channel, string[] users);
        public event NamesEvent OnNamesEvent;

        public static readonly Regex StripRegex = new(@"\x03(?:\d{1,2}(?:,\d{1,2})?)|[\u0000-\u0019]",
            RegexOptions.Compiled | RegexOptions.CultureInvariant);

        public Client(string username, string host, int port, int timeout, bool tls, bool ignoreInvalidCert, string commandPrefix)
        {
            this.username = username;
            this.host = host;
            this.port = port;
            this._tls = tls;
            this.timeout = timeout * 1000;
            this._ignoreInvalidCert = ignoreInvalidCert;
            this.prefix = commandPrefix;
            _connected = false;
        }

        public void SendCommand(string command)
        {
            if (!_connected) throw new Exception($"Failed to send command: '{command}', bot is not connected");
            _tx.WriteLine(command);
            _tx.Flush();
            logger.Debug("[--> IRC]: " + command);
        }

        private ParsedMessage ParseMessage(string input)
        {
            ParsedMessage pm = new ParsedMessage();
            string[] args;

            // checks for PING command 
            if (input[0] != ':')
            {
                args = input.Split(" ");
                pm.author = null;
                pm.command = args[0];
                pm.content = args[1].Substring(1);
                return pm;
            }
            int index = input.IndexOf(":", 1);
            if (index == -1)
            {
                args = input.Substring(1).Split(" ");
                pm.command = args[1];
                return pm;
            }
            args = input.Substring(1, index).Split(" ");
            if (args[1] == "353")
            {
                pm.command = args[1];
                pm.users = input.Substring(index + 1).Split(" ");
                pm.channel = args[4];
            }
            pm.command = args[1];
            pm.content = input.Substring(index + 1);
            index = args[0].IndexOf("!");
            if (index == -1)
            {
                return pm;
            }
            pm.channel = args[2];
            pm.author = args[0].Substring(0, index);
            return pm;
        }

        private bool ValidateSslCert(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            if (policyErrors == SslPolicyErrors.None)
            {
                logger.Debug("Certificate validation successful");
                logger.Debug("Certificate:\n" + certificate);
                return true;
            }
            if (_ignoreInvalidCert)
            {
                logger.Warn("Certificate validation failed, however ignored in config, continuing...");
                return true;
            }
            logger.Error("Certificate validation failed");
            logger.Error("certificate:\n" + certificate);
            return false;
        }

        public void Start()
        {
            while (!dConnected)
            {
                Task.Delay(500);
            }
            try
            {
                _client = new TcpClient();
                logger.Info($"Attempting to connect to {host}:{port}");
                if (!_client.ConnectAsync(host, port).Wait(timeout))
                {
                    throw new TimeoutException($"Failed to connect to {host}:{port} within {timeout} seconds");
                }
                logger.Info($"Successfully connected to {host}:{port}");
                _connected = true;
                if (_tls)
                {
                    try
                    {
                        SslStream stream = new SslStream(_client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateSslCert), null);
                        stream.AuthenticateAsClient(host);
                        _rx = new StreamReader(stream);
                        _tx = TextWriter.Synchronized(new StreamWriter(stream));
                    }
                    catch (System.Security.Authentication.AuthenticationException)
                    {
                        return;
                    }
                }
                else
                {
                    NetworkStream stream = _client.GetStream();
                    _rx = new StreamReader(stream);
                    _tx = TextWriter.Synchronized(new StreamWriter(stream));
                }
                SendCommand($"USER {username} 0 * :{username}");
                SendCommand($"NICK {username}");

                string input;
                while ((input = _rx.ReadLine()) != null)
                {
                    logger.Debug("[<-- IRC]: " + input);
                    ParsedMessage message = ParseMessage(input);
                    switch (message.command)
                    {
                        case "PING":
                            SendCommand($"PONG :{message.content}");
                            break;

                        case "001":
                            OnReady.Invoke();
                            break;

                        case "353":
                            OnNamesEvent.Invoke(message.channel, message.users);
                            break;

                        case "PRIVMSG":
                            message.content = StripRegex.Replace(message.content, string.Empty);
                            OnMessage.Invoke(message.author, message.channel, message.content);
                            if (message.content.StartsWith(prefix))
                            {
                                int i = message.content.IndexOf(" ");
                                if (i != -1)
                                {
                                    OnCommand.Invoke(message.author, message.channel, message.content[0], message.content.Substring(1, i - 1), message.content.Substring(i + 1));
                                }
                                else
                                {
                                    OnCommand.Invoke(message.author, message.channel, message.content[0], message.content.Substring(1), null);
                                }
                            }
                            break;

                        case "JOIN":
                            OnUserJoin.Invoke(message.author, message.content);
                            break;

                        case "PART":
                            OnUserPart.Invoke(message.author, message.channel, message.content);
                            break;

                        case "QUIT":
                            OnUserQuit.Invoke(message.author, message.content);
                            break;
                    }
                }
            }
            finally
            {
                _connected = false;
                logger.Info("Disconnected from IRC server, exiting...");
            }
        }
    }
}
