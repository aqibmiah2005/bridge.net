The following template is used if you have found a bug with Bridge.Net, if you want to suggest a new feature the suggestion template should be used, if you want to suggest and improvement for a existing feature, use the improvement template.

# Version and Logs
Please provide the version of Bridge.Net you are using, furthermore please attach your Latest.log which can be found in the working directory of the bot.

Additional support could be used using the Debug logger, which can be set manually using the config.toml and replacing the level with Debug instead of Info, however this will log everything it receives, and therefore you do not have as much privacy, therefore it is only a request and not mandatory.

# What did you expect to happen?
Please provide a full explanation of how you were using Bridge.Net, and what you expected to happen.

# What actually happened?
Please provide a full explanation of what actually happened when you attempted to use Bridge.Net.

# Steps to reproduce (A numbered list would be nice :) )

# Operating System:
Please provide the operating system you were using Bridge.Net.

# More info? (Optional!)

# Possible solutions? (Optional!)

