using System;

namespace Bridge
{
    public class Statistics
    {
        // Irc statistics
        public int ircMessages { get; set; }
        public int ircMessageChars { get; set; }
        public int ircMessageWords { get; set; }

        // Discord statistics
        public int discordMessages { get; set; }
        public int discordMessageChars { get; set; }
        public int discordMessageWords { get; set; }

        public Statistics()
        {
            this.ircMessages = 0;
            this.ircMessageChars = 0;
            this.ircMessageWords = 0;

            this.discordMessages = 0;
            this.discordMessageChars = 0;
            this.discordMessageWords = 0;
        }

        public double AverageDiscordChars() // Finds the average number of characters in discord messages received
        {
            try
            {
                return Math.Round((double)discordMessageChars / discordMessages, 2);
            }
            catch (System.DivideByZeroException)
            {
                return 0;
            }
        }

        public double AverageDiscordWords() // Finds the average number of words in discord messages received
        {
            try
            {
                return Math.Round((double)discordMessageWords / discordMessages);
            }
            catch (System.DivideByZeroException)
            {
                return 0;
            }
        }

        public double AverageIrcChars() // Finds the average number of characters in IRC messages received
        {
            return Math.Round((double)ircMessageChars / ircMessages, 2);
        }

        public double AverageIrcWords() // Finds the average number of words in IRC messages received
        {
            return Math.Round((double)ircMessageWords / ircMessages, 2);
        }

        public static int CalculateWords(string msg)
        {
            int words = 1;
            for (int i = 0; i < msg.Length; i++)
            {
                if (msg[i] == ' ')
                {
                    words++;
                }
            }
            return words;
        }
    }
}
