using Discord;
using Discord.WebSocket;
using static Bridge.Program;

namespace Bridge
{
    public class DiscordCommandHandler
    {
        public async static void Handle(string prefix, SocketMessage message)
        {
            // get the index of the first space which is the index after the final character of the command
            int index = message.Content.IndexOf(" ");

            // Fetch the command after the prefix
            string command;
            if (index == -1)
            {
                command = message.Content.Substring(prefix.Length).ToLower();
            }
            else
            {
                command = message.Content.Substring(prefix.Length, index - 1).ToLower();
            }

            EmbedBuilder builder = new EmbedBuilder();
            switch (command) // Check if the command exists, if not default will be called
            {
                case "help":
                    builder.WithTitle("Commands:");
                    builder.AddField("Help", "A command which displays all the other commands (this command)", false);
                    builder.AddField("users", "A command which displays all the users within a channel you specify (users <channel prefix> <channel>)");
                    builder.WithColor(Color.Green);
                    await message.Channel.SendMessageAsync(embed: builder.Build());
                    break;

                case "users":
                    long channel = (long)message.Channel.Id;
                    string ircChannel = null;
                    foreach (Bridge bridge in bridges)
                    {
                        if (bridge.discordChannel == channel)
                        {
                            ircChannel = bridge.ircChannel;
                        }
                    }
                    if (ircChannel == null)
                    {
                        break;
                    }
                    ircClient.SendCommand("NAMES " + ircChannel);
                    break;

                case "stats":
                    builder.WithTitle("Bridge Statistics");
                    builder.AddField("Discord Messages", $"`{stats.discordMessages}`", true);
                    builder.AddField("Average Discord Words per Message", $"`    {stats.AverageDiscordWords()}    `", true);
                    builder.AddField("Average Discord Characters per Message", $"`    {stats.AverageDiscordWords()}    `", true);
                    builder.AddField("IRC messages", $"`{stats.ircMessages}`", true);
                    builder.AddField("Average IRC Words per Message", $"`    {stats.AverageDiscordWords()}    `", true);
                    builder.AddField("Average IRC Characters per Message", $"`    {stats.AverageDiscordChars()}    `", true);
                    builder.WithColor(Color.Green);
                    await message.Channel.SendMessageAsync(embed: builder.Build());
                    break;

                case "command":
                    foreach (Bridge bridge in bridges)
                    {
                        if (bridge.discordChannel == (long)message.Channel.Id)
                        {
                            string msg = message.Content;
                            int i = msg.IndexOf(" ");
                            ircClient.SendCommand($"PRIVMSG {bridge.ircChannel} :{msg.Substring(i + 1)}");
                        }
                    }
                    break;

                default:
                    builder.WithTitle("Invalid command");
                    builder.WithDescription($"The command '{command}' could not be found");
                    builder.WithColor(Color.Red);
                    await message.Channel.SendMessageAsync(embed: builder.Build());
                    break;
            }
        }
    }
}
